<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
	/**
	* @const URL - URL-адрес вендора
	*/

	const URL = 'http://phisix-api3.appspot.com/stocks.json' ;

	/**
	* Загрузка списка валют в формате JSON
	* 
	* @throws \Exception
	* @return array
	*/

	public static function getAll( ) {
		$reader = new \pcrov\JsonReader\JsonReader( ) ;
		$reader->open( self::URL ) ;
		$reader->read( 'stock' ) ;
		$reader->read( ) ;

		$result = array( ) ;

		do {
			$item = $reader->value( ) ;

			if ( empty( $item[ 'volume' ] ) ) {
				continue ;
			}

			$result[] = array(
				'name' => $item[ 'name' ] ,
				'volume' => $item[ 'volume' ] ,
				'amount' => sprintf( '%.02f' , $item[ 'price' ][ 'amount' ] )
			) ;
		} while ( $reader->next( ) ) ;

		$reader->close( ) ;

		return $result ;
	}
}
