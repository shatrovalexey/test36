<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response ;
use App\Currency as ModelCurrency ;

class Currency extends Controller
{
	/**
	* Вывести список валют в формате JSON
	* 
	* @throws \Exception
	* @return Response
	*/

	public function __invoke( ) {
		/**
		* @var array $result - результирующие данные
		*/

		$result = ModelCurrency::getAll( ) ;

		return response( )->json( $result ) ;
	}
    //
}
