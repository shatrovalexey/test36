jQuery( function( ) {
	var $timeout = 15e3 ;
	var $URL = "ws://" + location.hostname + ":8001" ;
	var $table = jQuery( ".data-table" ) ;
	var $tbody = $table.find( "tbody" ) ;
	var $tr = $tbody.find( "tr:first" ).clone( true ) ;
	var $reload_running = false ;
	var $make_request = function( ) {
		if ( $reload_running ) {
			return false ;
		}

		$reload_running = true ;
		$socket.send( ' ' ) ;

		return true ;
	} ;
	var $reload = function( $evt ) {
		var $data = JSON.parse( $evt.data ) ;

		$tbody.empty( ) ;

		jQuery( $data ).each( function( $i , $item ) {
			var $trNode = $tr.clone( true ) ;

			for( var $key in $item ) {
				$trNode.find( "[data-name=" + $key + "]" ).text( $item[ $key ] ) ;
			}

			$tbody.append( $trNode ) ;
		} ) ;

		$reload_running = false ;
		setTimeout( $make_request , $timeout ) ;
	} ;

	var $socket = new WebSocket( $URL ) ;

	$socket.onmessage = $reload ;
	$socket.onopen = $make_request ;

	$table.find( ".data-table-reload" ).click( $socket.onmessage ).click( ) ;

} ) ;