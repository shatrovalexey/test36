jQuery( function( ) {
	var $table = jQuery( ".data-table" ) ;
	var $tbody = $table.find( "tbody" ) ;
	var $tr = $tbody.find( "tr:first" ).clone( true ) ;
	var $reload_running = false ;
	var $timeout = 15e3 ;
	var $reload = function( $force ) {
		if ( $reload_running ) {
			return false ;
		}

		$reload_running = true ;
		jQuery.ajax( {
			"url" : $table.data( "set" ) ,
			"dataType" : "json" ,
			"success" : function( $data ) {
				$reload_running = false ;
				$tbody.empty( ) ;

				jQuery( $data ).each( function( $i , $item ) {
					var $trNode = $tr.clone( true ) ;

					for( var $key in $item ) {
						$trNode.find( "[data-name=" + $key + "]" ).text( $item[ $key ] ) ;
					}

					$tbody.append( $trNode ) ;
				} ) ;

				setTimeout( function( ) { $reload( ) ; } , $timeout ) ;
			} ,
			"failure" : function( ) {
				$reload_running = false ;
				$reload( ) ;
			}
		} ) ;

		return true ;
	} ;

	$table.find( ".data-table-reload" ).click( function( ) {
		$reload( ) ;
	} ).click( ) ;
} ) ;