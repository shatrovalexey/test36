<!DOCTYPE html>

<html>
	<head>
		<title>Task 36</title>
		<meta charset="utf-8">

		<link rel="stylesheet" href="/css/app.css" type="text/css">
		<link rel="stylesheet" href="/css/style.css" type="text/css">
		<script src="/js/app.js"></script>
		<script src="/js/script-http.js"></script>
	</head>
	<body>
		<table class="data-table" data-set="/data">
			<caption>
				<button class="data-table-reload">обновить данные</button>
				<span>Список валют</span>
			</caption>
			<thead>
				<tr>
					<th>Название</th>
					<th>Объём</th>
					<th>Цена</th>
				</tr>
			</thead>
			<tbody>
				<tr class="nod">
					<td data-name="name"></td>
					<td data-name="volume"></td>
					<td data-name="amount"></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>