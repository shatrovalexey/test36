*** ЗАДАЧА ***
1.	Разработать Главный экран (веб-страница) - представляет собой таблицу - список валют. Список валют нужно получать по ссылке: http://phisix-api3.appspot.com/stocks.json
2.	Ячейка валюты - название валюты, цена и количество. Название валюты - поле "name", цена - поле "volume", количество - поле "amount".Volumeотображается как целое число. Amount - 2 знака после запятой. Высота ячейки - 50.
3.	Данные в таблице должны обновляться каждые 15 секунд.
4.	Также должна быть кнопка в ручного обновления данных в навигационном меню справа. 
Ответ: на выходе предоставить исходный код на PHP (laravel) в gitрепозитории.

*** ЗАПУСК ***
`\
	'git' 'clone' 'https://bitbucket.org/shatrovalexey/test36' && \
	'cd' 'test36' && \
	'composer' 'install' && \
	'cp' '~/.env.example' '.env' && \
	'php' 'artisan' 'key:generate' && \
	'php' 'artisan' 'serve' & && \
	'php' 'artisan' 'socket:serve' & ;
` ;

Если нет '~/.env.example', то его можно скопировать из https://github.com/laravel/laravel/blob/master/.env.example.
При запуске `'php' 'artisan' 'serve'` можно дополнительно указать опцию ` --host=hostname`.

Вариант реализации для протокола HTTP: http://localhost:8000/
Вариант реализации для протокола WebSocket: http://localhost:8000/ws

*** АВТОР ****
Шатров Алексей <mail@ashatrov.ru>